
from typing import List
import random
from functools import cmp_to_key
import math

from structs import Company, Pool, Report
from generate import generate_companies


thresholds = (
    0.005,
    0.010,
    0.015,
    0.018,
    0.019,
    0.020,
)

MIN_DELAY_MIN = 10

delays_min = tuple(int(MIN_DELAY_MIN * math.log2(float(1 + x))**2)
                   for x in range(len(thresholds), 0, -1))

DEFAULT_DELAY_MIN = max(delays_min) * 2


def analyze(company: Company, pools: List[Pool]):
    problems = {}
    tmp = pools.copy()
    for threshold in sorted(thresholds):
        tmp = [p for p in tmp if p.broken / p.devices >= threshold]
        problems[threshold] = tmp.copy()
    return Report(company, problems)


def compare(report1, report2):
    for threshold in sorted(thresholds, reverse=True):
        if len(report1.problems[threshold]) > len(report2.problems[threshold]):
            return 1
        if len(report1.problems[threshold]) < len(report2.problems[threshold]):
            return -1
    return 0


def prioritize(reports):
    return sorted(reports, key=cmp_to_key(compare), reverse=True)


def get_delay(report: Report):
    for i, threshold in sorted(enumerate(thresholds),
                               key=lambda v: v[1], reverse=True):
        if len(report.problems[threshold]) > 0:
            return delays_min[i]
    return DEFAULT_DELAY_MIN


if __name__ == '__main__':
    reports = [analyze(company, pools) for company, pools in
               generate_companies(100)]
    report = prioritize(reports)[-1]
    print(report.company)
    print(delays_min)
    print(f"delay: {get_delay(report)} minutes")
    for threshold, pools in report.problems.items():
        print(threshold, len(pools))
