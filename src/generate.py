import random
import math
from typing import List, Union

from structs import Pool, Company


MIN_POOLS_NUMBER = 10
MAX_POOLS_NUMBER = 1000

MIN_DEVICES = 100
MAX_DEVICES = 10000000

MIN_BROKEN_PORTION = 0.001
MAX_BROKEN_PORTION = 0.020


def generate_devices() -> Union[int, int]:
    devices_number = random.randint(MIN_DEVICES, MAX_DEVICES)
    broken_portion = random.uniform(MIN_BROKEN_PORTION, MAX_BROKEN_PORTION)
    return devices_number, math.floor(devices_number * broken_portion)


def generate_copmany_data(company_id) -> Union[Company, List[Pool]]:
    pools_number = random.randint(MIN_POOLS_NUMBER, MAX_POOLS_NUMBER)

    pools = []
    for pool_id in range(1, pools_number + 1):
        devices, broken = generate_devices()
        pools.append(Pool(company_id, pool_id, devices, broken))

    return Company(company_id, None), pools


def generate_companies(number):
    for company_id in range(1, number + 1):
        yield generate_copmany_data(company_id)


if __name__ == '__main__':
    for company, pools in generate_companies(1):
        print(company)
        print(pools)
