import os


POSTGRES_HOST = os.environ.get('POSTGRES_HOST', 'localhost')
POSTGRES_PORT = os.environ.get('POSTGRES_PORT', '5432')
POSTGRES_USER = os.environ.get('POSTGRES_USER', 'myuser')
POSTGRES_PASSWORD = os.environ.get('POSTGRES_PASSWORD', 'mysecretpassword')
POSTGRES_DB = os.environ.get('POSTGRES_DB', 'taskdb')
