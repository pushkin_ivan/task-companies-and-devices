from collections import namedtuple


Company = namedtuple('Company', ['id', 'updated'])

Pool = namedtuple(
    'Pool', [
        'company_id', 'company_pool_id', 'devices', 'broken'])

Report = namedtuple('Report', ['company', 'problems'])
